import Head from 'next/head'
import Link from 'next/link'
import Layout, { siteTitle } from "../layout/layout"
import utilStyles from '../styles/utils.module.css'
import  { getSortedPostsData } from '../lib/posts'
import Date from '../components/date'
import Button from 'react-bootstrap/Button';

export default function Home( {allPostsData}) {
  return (
    <Layout home>
    <Head>
        <title>{siteTitle}</title>
    </Head>
    <section className={utilStyles.headingMd}>
        <Button color="primary">
            TEST Bootstrapt
        </Button>
    </section>
    {/*<section className={utilStyles.headingMd}>
        <p>Hi! I'm Gabriel</p>
    </section>
    <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
        <h2 className={utilStyles.headingLg}>Blog</h2>
        <ul className={utilStyles.list}>
            {allPostsData.map(({ id, date, title }) => (
                <li className={utilStyles.listItem} key={id}>
                    <Link href="/posts/[id]" as={`/posts/${id}`}>
                        <a>{title}</a>
                    </Link>
                    <br />
                    <small className={utilStyles.lightText}>
                        <Date dateString={date}/>
                    </small>
                </li>
            ))}
        </ul>
    </section>*/}
</Layout>
  )
}

export async function getStaticProps() {
  const allPostsData = getSortedPostsData()
  return {
      props: {
          allPostsData
      }
  }
}