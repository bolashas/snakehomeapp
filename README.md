# SnakeHomeApp

### Context

The main point of this project is to try out React with NEXT.js developpement.

The final result would be like a dashboard while trying material-ui components from scratch.

It's true that we could have use a dashboard template, but we really want to try it out from scratch.
* Example of existing dashboard we'll try to produce :
* [ArchitechUI with Bootstrap](https://dashboardpack.com/live-demo-preview/?livedemo=113%3Futm_source%3Dcolorlib&utm_medium=reactlist&utm_campaign=architectreact&v=1ee0bf89c5d1)
* [Devias Kit with Material-UI](https://material-ui.com/store/previews/devias-kit-pro/)


### Creators

| Name            | BirthDate     | Residence   |
| --------------- |---------------|-------------|
| Joao Pegacho    | 1995          | PT          |   
| Luis Pegacho    | 1995          | PT          |
| Gabriel Cristao | 1996          | CH          |
